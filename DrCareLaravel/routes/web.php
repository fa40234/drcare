<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::get('/adminpanel', function(){
    return view('adminpanel');
});

Route::get('/doctorpanel',function (){
    return view('doctorpanel');
})->name('doctorpaneli');

Route::get('/docappointment/{id}', 'DoctorController@display3')->name('docappointment');


Route::get('/search','DoctorController@search');
Route::resource('posts','DoctorController@search');


Route::get('/users_list', 'UserController@display')->name('users_list');
Route::get('/doc_list', 'DoctorController@display2')->name('doctors_list');
Route::name('delete_doctor')->delete('/doc_list/{id}', 'DoctorController@destroy');

Route::name('delete_user')->delete('/users_list/{id}', 'UserController@destroy');
Route::get('/home', 'DoctorController@index')->name('home');

Route::get('/comments_list', 'CommentsController@display')->name('comments_list');
Route::name('delete_comment')->delete('/comments_list/{id}', 'CommentsController@destroy');
Route::resource('specialties', 'SpecialtyController');
Route::get('specialties', 'SpecialtyController@display');
Route::resource('register', 'HomeController');


Route::get('/department', function () {
    return view('department');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/appointment', function () {
    return view('appointment');
});
Route::get('/about', function () {
    return view('about');
});
Route::post('/toggle-approve', 'AppointmentController@approval')->name('toggle-approve');

Route::get('/info', function () {
    return view('info');
});

Auth::routes();
//Doctor Authenticatation Routes
Route::post('login/doctor','Auth\LoginController@doctorLogin')->name('doctor.login');
Route::post('logout/doctor','Auth\LoginController@doctorLogout')->name('doctor.logout');
//End Doctor Authenticatation Routes
Route::get('/doctorProfile', function () {
    return view('doctorProfile');
});

Route::get('/medicalid', function () {
    return view('medicalid');
})->name('medicalid');

Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
//Route::get('login/doctor', 'Auth\LoginController@showDoctorLoginForm')->name('doctor.login');
//Route::post('login/doctor', 'Auth\LoginController@doctorLogin')->name('doctor.login.submit');
//

//Route::get('/doctor/email/verify', function () {
//    return view('auth.verifyDoctor');
//});
Route::get('/profile', 'HomeController@showprofile')->name('profile');
Route::get('edit', 'UserController@edit');
Route::patch('profile/edit/{id}', 'UserController@update')->name('edit.profile');


Auth::routes(['verify' => true]);

Auth::routes();

Route::get('/login/{social}', 'SocialAuthGoogleController@redirect')
    ->where('social', 'google|facebook');
//Route::get('/callback', 'SocialAuthGoogleController@callback');
Route::get('/login/{social}/callback', 'SocialAuthGoogleController@callback')
    ->where('social', 'google|facebook');

Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');


Route::get('/careprogram', 'BlogsController@display');

Route::post('comment/{id}','CommentsController@store')->name('comment.store');
Route::post('medicalid/{id}','MedicalIdController@store')->name('medic.store');
Route::post('doctor/{id}','AppointmentController@store')->name('appointment.store');

Route::name('blogs_path')->get('/blogs', 'BlogsController@index');
Route::name('create_blog_path')->get('/blogs/create', 'BlogsController@create');
Route::name('store_blog_path')->post('/blogs', 'BlogsController@store');
Route::name('blog_path')->get('/blogs/{id}', 'BlogsController@show');
Route::name('edit_blog_path')->get('/blogs/{id}/edit', 'BlogsController@edit');
Route::name('update_blog_path')->put('/blogs/{id}', 'BlogsController@update');
Route::name('delete_blog_path')->delete('/blogs/{id}', 'BlogsController@destroy');
Route::post('/reply/store', 'CommentsController@replyStore')->name('reply.add');

Route::get('/doctor', 'DoctorController@display');
Route::name('doctorProfile_path')->get('/doctor', 'DoctorController@indexx');
//Route::post('/doctorProfile', 'DoctorController@store');
Route::name('doctorProfile_path')->get('/doctor/{id}', 'DoctorController@show');
//Route::get('/doctorProfile/blog/{id}', 'DoctorController@edit');
//Route::put('/doctorProfile/{id}', 'DoctorController@update');
//Route::delete('/doctorProfile/{id}', 'DoctorController@destroy');

Route::post('/contact', 'ContactFormController@store')->name('kontakti');
Route::get('/contacts/list', 'ContactFormController@display')->name('displayKontakt');
