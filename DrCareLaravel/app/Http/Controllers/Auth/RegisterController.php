<?php

namespace App\Http\Controllers\Auth;

use App\Doctor;
use App\Http\Controllers\Controller;
use App\Specialties;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:doctor');
    }

    public function showRegistrationForm() {
        $specialties = Specialties::all();

        return view('auth.register', compact('specialties'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if (Request::get('registerUser')) {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'birthday' => ['required', 'date'],
                'address' => ['required'],
                'phone_nr' => ['required', 'numeric', 'min:9'],
                'personalNumber' => ['required', 'numeric', 'min:9']

            ]);

        } else if (Request::get('registerDoctor')) {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'birthday' => ['required', 'date'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'address' => ['required'],
                'phone_nr' => ['required', 'numeric', 'min:9']
            ]);
        }

    }


    protected function create(array $data)
    {
        if (Request::get('registerUser')) {
            $path = Storage::putFile('public', request()->file('photo'));
            $url = Storage::url($path);
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'birthday' => $data['birthday'],
                'address' => $data['address'],
                'phone_nr' => $data['phone_nr'],
                'personalNumber' => $data['personalNumber'],
                'photo' => $url
            ]);


        } else if (Request::get('registerDoctor')) {
//            $path = Storage::putFile('public', request()->file('photo'));
////        $url = Storage::url($path);
            return Doctor::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'birthday' => $data['birthday'],
                'spec' => $data['spec'],
                'phone_nr' => $data['phone_nr'],
                'address' => $data['address'],
                'password' => Hash::make($data['password'])
//                'photo' => $url
            ]);


        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(\Illuminate\Http\Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 201)
            : redirect($this->redirectPath())->with('warning','You need to confirm your account.  Please check your email for verification.');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        if (Request::get('registerUser')) {
            return Auth::guard();
        } else if (Request::get('registerDoctor')) {
            return Auth::guard('doctor');
        }
    }


}
//            if (request()->hasFile('photo')) {
//
//                $photo = request()->file('photo')->getClientOriginalName();
//                request()->file('photo')->storeAs('images', $doctor->id . '/' . $photo, '');
//                $doctor->update(['photo' => $photo]);
//            }
//            return $doctor;
