<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Specialties;
use Illuminate\Http\Request;
use App\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }


    public function showprofile(){
        return view('profile');
    }

    public function showProfileDoctor(){
        return view('doctorProfile');
    }
}
