<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {

//        return $this->successResponse($this->userService->obtainUser($id));
        $user = User::findorFail($id);
        return response()->json($user, 200);
    }

    public function edit()
    {
//        $user = User::find($id);
        return view('editProfile');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'email',
            'password',
            'gender',
            'birthday' => ['required', 'date'],
            'address' => ['required'],
            'phone_nr' => ['required', 'numeric', 'min:9'],
//            'personalNumber' => ['required', 'numeric', 'min:9'],
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
//        $user->email = $request->get('email');
//        $user->password = $request->get('password');
        $user->gender = $request->get('gender');
        $user->birthday = $request->get('birthday');
        $user->address = $request->get('address');
        $user->phone_nr = $request->get('phone_nr');
//        $user->personalNumber = $request->get('personalNumber');
        $user->save();
        return redirect()->route('profile');
    }

    public function display(){
        //example usage.
        $user = User::all();

        return view('userslist', compact('user'));
    }

    public function destroy($id){
        $user = User::find($id);

        $user->delete();

        return redirect()->route('users_list')->with('message','User deleted successfully .');
    }

}
