<?php

namespace App\Http\Controllers;

use App\Specialties;
use Illuminate\Http\Request;

class SpecialtyController extends Controller
{

    public function index(){
        return view('specialties_add');
    }

    public function store(Request $request){
        $specialties = new Specialties;
        $specialties->specialty = $request->specialty;
        $specialties->save();
        return redirect()->back()->with('message','Specialty added successfully.');
    }

    public function display(){
        //example usage.
        $specialties = Specialties::all();
        return view('specialties_add', compact('specialties'));
    }


}
