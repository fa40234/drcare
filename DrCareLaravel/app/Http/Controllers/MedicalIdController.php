<?php

namespace App\Http\Controllers;

use App\MedicalId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MedicalIdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $medicalid = new MedicalId();
        $medicalid->users_id = Auth::id();
        $medicalid->medicalcond = $request->medicalcond;
        $medicalid->medications = $request->medications;
        $medicalid->bloodtype = $request->bloodtype;
        $medicalid->emergencycont = $request->emergencycont;
        $medicalid->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MedicalId  $medicalId
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalId $medicalId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MedicalId  $medicalId
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalId $medicalId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MedicalId  $medicalId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicalId $medicalId)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MedicalId  $medicalId
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalId $medicalId)
    {
        //
    }
}
