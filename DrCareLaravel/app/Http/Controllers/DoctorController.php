<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function indexx()
    {

        $doctors = Doctor::all();

        return view('doctor', ['doctors' => $doctors]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $doctors = Doctor::all();
        return view('index', ['doctors' => $doctors]);
    }

    public function show($id)
    {
        $doctor = Doctor::find($id);

        return view('doctorProfile', ['doctor' => $doctor]);
    }


    public function edit($id)
    {

        $doctor = Doctor::find($id);

        return view('doctorEdit', ['doctor' => $doctor]);

    }

    //update method implementation later


    public function destroy($id)
    {
        $doctor = Doctor::find($id);

        $doctor->delete();

        return redirect()->route('doctors_list')->with('message','User deleted successfully.');
    }

    public function display()
    {

        $doctors = Doctor::all();

        return view('doctor', ['doctors' => $doctors]);
    }

    public function display2()
    {

        $doctors = Doctor::all();

        return view('doctorslist', ['doctors' => $doctors]);
    }

    public function display3($id){
        $userId = Auth::id();
        //$appointments = Appointment::where('doctor_id', '=', $userId)->firstOrFail();
        $appointments = Appointment::all();
    // $appointments = Appointment::where('doctor_id', $id);

      //  $appointments = Appointment::where('id' ,'=' ,$id);

        return view('doctorpanel', ['appointments' => $appointments]);
    }

    public function search(Request $request){
        $search = $request->get('search');
        $posts = DB::table('doctors')->where('name','like','%'.$search. '%')->
        orWhere('spec','like','%'.$search.'%')->paginate(5);
        return view('doctor',['doctors'=>$posts]);
    }


}
