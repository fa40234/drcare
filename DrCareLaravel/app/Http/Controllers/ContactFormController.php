<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactForm;

class ContactFormController extends Controller
{

    public function store(Request $request)
    {
        $newsEmail = new ContactForm();
        $newsEmail->name=$request->name;
        $newsEmail->email=$request->email;
        $newsEmail->subject=$request->subject;
        $newsEmail->message=$request->message;
        $newsEmail->save();
        return back()->with('message', 'Faleminderit qe na kontaktuat');
    }

    public function display(){
        //example usage.
        $newsEmail = ContactForm::all();

        return view('contacts', compact('newsEmail'));
    }

}
