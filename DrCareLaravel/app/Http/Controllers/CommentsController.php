<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yoeunes\Toastr\Facades\Toastr;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);
        $comment = new Comment();
        $comment->users_id = Auth::id();
        $comment->blog_id = $id;
        $comment->comment = $request->comment;
        $comment->save();
        Toastr::success('Comment Successfully Published :)', 'Success');
        return redirect()->back();
    }

//    public function replyStore(Request $request)
//    {
//        $reply = new Comment();
//        $reply->comment = $request->comment;
//        $reply->user()->associate($request->user());
//        $reply->parent_id = $request->get('comment_id');
//        $post = Blog::find($request->get('post_id'));
//
//        $post->comments()->save($reply);
//
//        return back();
//
//    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function display(){
        //example usage.
        $comment = Comment::all();

        return view('comments_list', compact('comment'));
    }

    public function destroy($id){
        $comment = Comment::find($id);

        $comment->delete();

        return redirect()->route('comments_list')->with('message','Comment deleted successfully .');
    }
}
