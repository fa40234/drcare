<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Foundation\Auth\VerifiesEmails;


class SocialAuthGoogleController extends Controller
{

    public function redirect($social)
    {
        return Socialite::driver($social)->redirect();
    }


    public function callback($social)
    {
        try {


            $socialUser = Socialite::driver($social)->stateless()->user();
            $existUser = User::where('email', $socialUser->email)->first();


            if ($existUser) {
                Auth::loginUsingId($existUser->id);
            } else {
                $user = new User;
                $user->name = $socialUser->name;
                $user->email = $socialUser->email;
                $user->google_id = $socialUser->id;
                $user->password = md5(rand(1, 10000));
//                $user->gender = $socialUser->gender;
                $user->markEmailAsVerified();
                $user->save();
                Auth::loginUsingId($user->id);

            }
            return redirect()->to('/home');
        } catch (Exception $e) {
            return 'error  SocialAuthGoogleController!!';
        }
    }
}
