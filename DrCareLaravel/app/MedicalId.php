<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalId extends Model
{
    protected $table = 'medical_ids';

    public function user(){
        $this->belongsTo(User::class);
    }
}
