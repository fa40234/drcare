<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [
        'id',
//        'parent_id',
        'comment',
        'blog_id',
        'users_id'
    ];


    public function blog(){
        return $this->belongsTo('App\Blog');
    }


     public function user(){
        return $this->belongsTo('App\User','users_id');
    }

    public function replies() {
        return $this->hasMany('App\Comment');
    }


}
