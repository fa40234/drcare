<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('users_id')
                ->constrained()
                ->onDelete('cascade');
            $table->foreignId('doctor_id')
                ->constrained()
                ->onDelete('cascade');
            $table->string('emri');
            $table->string('mbiemri')->nullable();
            $table->date('data');
            $table->string('arsyeja')->nullable();
            $table->string('adresa')->nullable();
            $table->string('mosha')->nullable();
            $table->string('address')->nullable();
            $table->boolean('approve')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
