<!DOCTYPE html>

<html lang="en">
<head>
    @include('layouts.headimp')
</head>
<body>
@include('layouts.header')
<!-- END nav -->

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">Add Medical id</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i class="ion-ios-arrow-forward"></i></a></span>
                    <span>Medical id <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

{{--@if (Auth::guest())--}}
{{--    <h2>You can't add medical id if youre not registered</h2>--}}
{{--@else--}}
    <section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
        <div class="container">
            <div class="row d-flex align-items-stretch no-gutters">
                <div class="col-md-6 p-4 p-md-5 order-md-last bg-light">
                    <form method="post" action="{{route('medic.store',Auth::id())}}" >

                        @csrf
                        <div class="form-group">
                            <input type="text" name="medicalcond" class="form-control" placeholder="Medical Condition">
                        </div>
                        <div class="form-group">
                            <input type="text" name="medications" class="form-control" placeholder="Medications">
                        </div>
                        <div class="form-group">
                            <input type="text" name="bloodtype" class="form-control" placeholder="Blood type">
                        </div>
                        <div class="form-group">
                            <input type="text" name="emergencycont" class="form-control" placeholder="Emergency contact">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary py-3 px-5">Add medical id</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
{{--@endif--}}




@include('layouts.footer')
<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#7dc2af"/>
    </svg>
</div>


@include('layouts.scriptimp')

</body>
</html>
