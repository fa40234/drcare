<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.headimp')
</head>
<body>
@include('layouts.header')
<!-- END nav -->

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">Clinical Department</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i class="ion-ios-arrow-forward"></i></a></span>
                    <span>Department <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <span class="subheading">Departments</span>
                <h2 class="mb-4">Clinic Departments</h2>
                <p>In this website you can see all departments and we will inform you about them</p>
            </div>
        </div>
        <div class="ftco-departments">
            <div class="row">
                <div class="col-md-12 nav-link-wrap">
                    <div class="nav nav-pills d-flex text-center" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="nav-link ftco-animate active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1"
                           role="tab" aria-controls="v-pills-1" aria-selected="true">Neurology</a>

                        <a class="nav-link ftco-animate" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2"
                           role="tab" aria-controls="v-pills-2" aria-selected="false">Surgical</a>

                        <a class="nav-link ftco-animate" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3"
                           role="tab" aria-controls="v-pills-3" aria-selected="false">Dental</a>

                        <a class="nav-link ftco-animate" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4"
                           role="tab" aria-controls="v-pills-4" aria-selected="false">Ophthalmology</a>

                        <a class="nav-link ftco-animate" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5"
                           role="tab" aria-controls="v-pills-5" aria-selected="false">Cardiology</a>

                    </div>
                </div>
                <div class="col-md-12 tab-wrap">

                    <div class="tab-content bg-light p-4 p-md-5 ftco-animate" id="v-pills-tabContent">

                        <div class="tab-pane py-2 fade show active" id="v-pills-1" role="tabpanel"
                             aria-labelledby="day-1-tab">
                            <div class="row departments">
                                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                                    <div class="img d-flex align-self-stretch"
                                         style="background-image: url(images/dept-1.jpg);"></div>
                                </div>
                                <div class="col-lg-8">
                                    <h2>Neurological Deparments</h2>

                                    <div class="row mt-5 pt-2">
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-first-aid-kit"></span></div>
                                                <div class="text">
                                                    <h3>Primary Care</h3>
                                                    <p>Get primary care with us</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-dropper"></span></div>
                                                <div class="text">
                                                    <h3>Lab Test</h3>
                                                    <p>Get best lab test with us </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-experiment-results"></span></div>
                                                <div class="text">
                                                    <h3>Care Program</h3>
                                                    <p>Get peace of mind in managing your health</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-heart-rate"></span></div>
                                                <div class="text">
                                                    <h3>Heart Rate</h3>
                                                    <p>Chek your heart rate </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-day-2-tab">
                            <div class="row departments">
                                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                                    <div class="img d-flex align-self-stretch"
                                         style="background-image: url(images/dept-2.jpg);"></div>
                                </div>
                                <div class="col-md-8">
                                    <h2>Surgical Deparments</h2>

                                    <div class="row mt-5 pt-2">
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-first-aid-kit"></span></div>
                                                <div class="text">
                                                    <h3>Primary Care</h3>
                                                    <p>Get primary care with us</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-dropper"></span></div>
                                                <div class="text">
                                                    <h3>Lab Test</h3>
                                                    <p>Get best lab test with us </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-experiment-results"></span></div>
                                                <div class="text">
                                                    <h3>Care Program</h3>
                                                    <p>Get peace of mind in managing your health</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-heart-rate"></span></div>
                                                <div class="text">
                                                    <h3>Heart Rate</h3>
                                                    <p>Chek your heart rate </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-day-3-tab">
                            <div class="row departments">
                                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                                    <div class="img d-flex align-self-stretch"
                                         style="background-image: url(images/dept-3.jpg);"></div>
                                </div>
                                <div class="col-md-8">
                                    <h2>Dental Deparments</h2>

                                    <div class="row mt-5 pt-2">
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-first-aid-kit"></span></div>
                                                <div class="text">
                                                    <h3>Primary Care</h3>
                                                    <p>Get primary care with us</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-dropper"></span></div>
                                                <div class="text">
                                                    <h3>Lab Test</h3>
                                                    <p>Get best lab test with us </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-experiment-results"></span></div>
                                                <div class="text">
                                                    <h3>Care Program</h3>
                                                    <p>Get peace of mind in managing your health</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-heart-rate"></span></div>
                                                <div class="text">
                                                    <h3>Heart Rate</h3>
                                                    <p>Chek your heart rate </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-day-4-tab">
                            <div class="row departments">
                                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                                    <div class="img d-flex align-self-stretch"
                                         style="background-image: url(images/dept-4.jpg);"></div>
                                </div>
                                <div class="col-md-8">
                                    <h2>Ophthalmology Deparments</h2>

                                    <div class="row mt-5 pt-2">
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-first-aid-kit"></span></div>
                                                <div class="text">
                                                    <h3>Primary Care</h3>
                                                    <p>Get primary care with us</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-dropper"></span></div>
                                                <div class="text">
                                                    <h3>Lab Test</h3>
                                                    <p>Get best lab test with us </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-experiment-results"></span></div>
                                                <div class="text">
                                                    <h3>Care Program</h3>
                                                    <p>Get peace of mind in managing your health</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-heart-rate"></span></div>
                                                <div class="text">
                                                    <h3>Heart Rate</h3>
                                                    <p>Chek your heart rate </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-day-5-tab">
                            <div class="row departments">
                                <div class="col-lg-4 order-lg-last d-flex align-items-stretch">
                                    <div class="img d-flex align-self-stretch"
                                         style="background-image: url(images/dept-5.jpg);"></div>
                                </div>
                                <div class="col-md-8">
                                    <h2>Cardiology Deparments</h2>

                                    <div class="row mt-5 pt-2">
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-first-aid-kit"></span></div>
                                                <div class="text">
                                                    <h3>Primary Care</h3>
                                                    <p>Get primary care with us</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-dropper"></span></div>
                                                <div class="text">
                                                    <h3>Lab Test</h3>
                                                    <p>Get best lab test with us </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-experiment-results"></span></div>
                                                <div class="text">
                                                    <h3>Care Program</h3>
                                                    <p>Get peace of mind in managing your health</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="services-2 d-flex">
                                                <div
                                                    class="icon mt-2 mr-3 d-flex justify-content-center align-items-center">
                                                    <span class="flaticon-heart-rate"></span></div>
                                                <div class="text">
                                                    <h3>Heart Rate</h3>
                                                    <p>Chek your heart rate </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('layouts.footer')
<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#7dc2af"/>
    </svg>
</div>


@include('layouts.scriptimp')

</body>
</html>
