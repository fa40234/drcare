@extends('layouts.app')

@if(!empty($successMsg))
    <div class="alert alert-success"> {{ $successMsg }}</div>
@endif

@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row justify-content-center">

                <table class="table table-striped table-light shadow-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Verified</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Address</th>
                        <th scope="col">Phone Nr</th>
                        <th scope="col">Personal Nr</th>
                        <th scope="col">User Type</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user as $useri)
                    <tr>

                            <td>{{$useri->id}}</td>
                        <td>{{$useri->name}}</td>
                        <td>{{$useri->email}}</td>
                        <td>{{$useri->email_verified_at->diffForHumans()}}</td>
                        <td>{{$useri->birthday}}</td>
                        <td>{{$useri->address}}</td>
                        <td>{{$useri->phone_nr}}</td>
                        <td>{{$useri->personalNumber}}</td>
                        <td>{{$useri->user_type}}</td>
                        <td>{{$useri->created_at->diffForHumans()}}</td>
                            <td>
                                <?php
                                if ($useri->user_type != "admin")
                                    echo '<button class="btn btn-outline-danger" data-target="#delete"data-toggle="modal" >Delete</button>';
                                ?>


                                </td>

                    </tr>
                    <div class="modal" id="delete" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you want to delete this user ?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="{{route('delete_user', [$useri->id])}}" method="post">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger" data-target="#delete" data-toggle="modal" >Delete</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </tbody>
                </table>



        </div>
    </div>

@endsection
