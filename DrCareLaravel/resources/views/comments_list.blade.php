@extends('layouts.app')


@section('content')


    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row justify-content-center">

            <table class="table table-striped table-light shadow-sm">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Comment</th>
                    <th scope="col">Blog Id</th>
                    <th scope="col">User Id</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Delete</th>

                </tr>
                </thead>
                <tbody>
                @foreach($comment as $com)
                    <tr>

                        <td>{{$com->id}}</td>
                        <td>{{$com->comment}}</td>
                        <td>{{$com->blog_id}}</td>
                        <td>{{$com->users_id}}</td>
                        <td>{{$com->created_at->diffForHumans()}}</td>

                        <td>
                            <button class="btn btn-outline-danger" data-target="#delete"data-toggle="modal" >Delete</button>
                        </td>

                    </tr>
                    <div class="modal" id="delete" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you want to delete this comment ?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="{{route('delete_comment', [$com->id])}}" method="post">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger" data-target="#delete" data-toggle="modal" >Delete</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
            </table>



        </div>
    </div>

@endsection
