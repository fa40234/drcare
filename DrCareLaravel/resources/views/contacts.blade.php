@extends('layouts.app')


@section('content')


    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row justify-content-center">

            <table class="table table-striped table-light shadow-sm">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Message</th>
                    <th scope="col">Created at</th>

                </tr>
                </thead>
                <tbody>
                @foreach($newsEmail as $com)
                    <tr>

                        <td>{{$com->id}}</td>
                        <td>{{$com->name}}</td>
                        <td>{{$com->email}}</td>
                        <td>{{$com->subject}}</td>
                        <td>{{$com->message}}</td>
                        <td>{{$com->created_at->diffForHumans()}}</td>

                    </tr>

                @endforeach
                </tbody>
            </table>



        </div>
    </div>

@endsection
