@extends('layouts.app')
@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @can('isAdmin')
                <div class="row">
                    <div class="col-sm">
                        <form  method="post" action="specialties">
                            @csrf
                            <label for="specialty">Specialty</label>
                            <div class="input-group">

                                <input type="text" name="specialty" class="form-control">

                                <button type="submit" class="btn btn-outline-primary">Add Specialty</button>

                            </div>

                        </form>
                    </div>
                    <div class="col-sm">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Specialties</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($specialties as $spec)
                            <tr>

                                <td scope="row">{{$spec->id}}</td>
                                <td>{{$spec->specialty}}</td>

                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>


        @endcan
        @guest
            <div class="alert alert-danger" role="alert">
                Pershendetje, ju nuk keni akses ne kete faqe.
            </div>
            <a href="/"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kthehuni prapa</button></a>
        @endguest
    </div>

@endsection
