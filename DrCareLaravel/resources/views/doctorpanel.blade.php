
<!DOCTYPE html>

<html lang="en">
<head>
    @include('layouts.headimp')
</head>
<body>
@include('layouts.header')
<!-- END nav -->

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">Appointments</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i class="ion-ios-arrow-forward"></i></a></span>
                    <span>Appointments <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
    <div class="container">
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="row d-flex align-items-stretch no-gutters">

                <div class="container">

                    <div class="row justify-content-center">

                        <table class="table table-striped table-light shadow-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Emri</th>
                                <th scope="col">Mbiemri</th>
                                <th scope="col">Data</th>
                                <th scope="col">Arsyeja</th>
                                <th scope="col">Adresa</th>
                                <th scope="col">Mosha</th>
                                <th scope="col">Approve</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appointments as $appointment)
                                @if($appointment->doctor_id == Auth::guard('doctor')->user()->id)
                                    @if(session()->has('message'))
                                        <?php if($appointment->approve == 1){
                                            echo '<div class="alert alert-success">';
                                           echo 'Takimi u aprovua';
                                        echo '</div>';
                                        }else{
                                            echo '<div class="alert alert-danger">';
                                            echo 'Takimi u disaprovua';
                                        echo '</div>';
                                        }
                                        ?>
                                    @endif
                                    <tr>

                                        <td>{{$appointment->doctor_id }}</td>
                                        <td>{{$appointment->emri}}</td>
                                        <td>{{$appointment->mbiemri}}</td>
                                        <td>{{$appointment->data}}</td>
                                        <td>{{$appointment->arsyeja}}</td>
                                        <td>{{$appointment->adresa}}</td>
                                        <td>{{$appointment->mosha}}</td>
                                        <td>
                                            <form action="{{'../toggle-approve'}}" method="POST">
                                                {{csrf_field()}}
                                                <input <?php if($appointment->approve == 1){echo "checked";}?> type="checkbox" name="approve">

                                                <input type="hidden" name="commentId" value="{{$appointment->id}}">

                                                <?php if($appointment->approve == 0){
                                                    echo '<input  class="btn btn-primary" type="submit" value="Aprovo">';
                                                }else{
                                                    echo '<input  class="btn btn-danger" type="submit" value="Disaprovo">';
                                                }
                                                ?>

                                            </form>

                                        </td>

                                        @endif
                                    </tr>

                                    @endforeach
                            </tbody>
                        </table>



                    </div>
                </div>


        </div>

    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
</section>



@include('layouts.footer')
<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#7dc2af"/>
    </svg>
</div>


@include('layouts.scriptimp')

</body>
</html>

