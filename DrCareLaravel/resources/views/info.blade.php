@extends('layouts.app')
<head>
    <style>
        body{
            background-color: #21D4FD;
            background-image: linear-gradient(19deg, #21D4FD 0%, #B721FF 100%);

        }
    </style>
</head>
@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">

                        <span class="input-group-text" id="inputGroup-sizing-default"><i class="fa fa-search"></i></span>
                    </div>


                    <input placeholder="Kerko emrin e doktorit" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">

                </div>
            </div>
            <div class="col-sm">
                <div class="input-group mb-3">

                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default"><i class="fa fa-map-marker"></i></span>
                    </div>


                    <input placeholder="Kerko lokacionin e doktorit" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
            </div>
            <div class="col-sm">
                <button type="button" class="btn btn-warning">Search</button>
            </div>
        </div>

    </div>

    <div class="container shadow-lg p-3 mb-5">
        <div class="card">
            <h5 class="card-header">Lista e Specializimeve</h5>
            <div class="card-body">
                <h5 class="card-title">Ne sistemin DrCare mund te gjeni keto lloj specializimesh</h5>
                <div class="row">
                    <div class="col-sm">
                        <div class="list-group list-group-flush">

                            <a href="#" class="list-group-item list-group-item-action">Primary care doctor</a>
                            <a href="#" class="list-group-item list-group-item-action">Dermatologist</a>
                            <a href="#" class="list-group-item list-group-item-action">Dentist</a>
                            <a href="#" class="list-group-item list-group-item-action">Psychiatrist</a>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="list-group list-group-flush">

                            <a href="#" class="list-group-item list-group-item-action">Ear, Nose & Throat Doctor</a>
                            <a href="#" class="list-group-item list-group-item-action">Pediatrist</a>
                            <a href="#" class="list-group-item list-group-item-action">Urologist</a>
                            <a href="#" class="list-group-item list-group-item-action">Gastroenterologist</a>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="list-group list-group-flush">

                            <a href="#" class="list-group-item list-group-item-action">Neorologist</a>
                            <a href="#" class="list-group-item list-group-item-action">Orthopedic Surgeon</a>
                            <a href="#" class="list-group-item list-group-item-action">Ophthalmotlogist</a>
                            <a href="#" class="list-group-item list-group-item-action">Pedritician</a>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="list-group list-group-flush">

                            <a href="#" class="list-group-item list-group-item-action">Eye Doctor</a>
                            <a href="#" class="list-group-item list-group-item-action">Therapist Counselor</a>
                            <a href="#" class="list-group-item list-group-item-action">Physical Therapist</a>

                            <a href="#" class="list-group-item list-group-item-action">Psychologist</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>



@endsection
