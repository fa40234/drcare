@extends('layouts.header')


@include('layouts.headimp')



@section('content')

    <link rel="stylesheet" href="/styles/profile.css">
    <section class="hero-wrap hero-wrap-2" style="background-image: url(images/bg_1.jpg);"
             data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">Register Form</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span>Register <i
                                class="ion-ios-arrow-forward"></i></span></p>
                </div>
            </div>
        </div>
    </section>
    <div style="margin-top:20px"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="col-md-6">
                        <div class="register-head">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user" role="tab"
                                       aria-controls="user" aria-selected="true">User</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="doctor-tab" data-toggle="tab" href="#doctor" role="tab"
                                       aria-controls="doctor" aria-selected="false">Register As Doctor</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    {{-- User Register Form --}}
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="user" role="tabpanel" aria-labelledby="user-tab">
                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-3">
                                    </div>

                                    <div class="form-group row">
                                        <label for="name"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                                   value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email"
                                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password"
                                                   required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="birthday"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Birthday') }}</label>

                                        <div class="col-md-6">
                                            <input id="birthday" type="date"
                                                   class="form-control @error('birthday') is-invalid @enderror"
                                                   name="birthday"
                                                   value="{{ old('birthday') }}" required autocomplete="birthday"
                                                   autofocus>

                                            @error('birthday')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="address"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address') }}" required autocomplete="address"
                                                   autofocus>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone_nr"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                        <div class="col-md-6">
                                            <input id="phone_nr" type="text"
                                                   class="form-control @error('phone_nr') is-invalid @enderror"
                                                   name="phone_nr"
                                                   value="{{ old('phone_nr') }}" required autocomplete="phone_nr"
                                                   autofocus>

                                            @error('phone_nr')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="personalNumber"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Personal Number') }}</label>

                                        <div class="col-md-6">
                                            <input id="personalNumber" type="text"
                                                   class="form-control @error('personalNumber') is-invalid @enderror"
                                                   name="personalNumber"
                                                   value="{{ old('personalNumber') }}" required
                                                   autocomplete="personalNumber"
                                                   autofocus>

                                            @error('personalNumber')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="photo"
                                               class="col-md-4 col-form-label text-md-right">{{__('Photo')}}</label>

                                        <div class="col-md-6">
                                            <input id="photo" type="file" class="file" name="photo">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary" name="registerUser"
                                                    value="registerUser">
                                                {{ __('Register') }}
                                            </button>

                                        </div>
                                    </div>
                                    <div class="form-group row mb-3">
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-6 col-form-label text-md-right">Or login with</label>
                                        <div class="row mb-5">
                                        </div>
                                        <div class="col-md-8 offset-md-4">
                                            <a href="{{ url('login/google') }}"
                                               class="btn btn-social-icon btn-google"><i
                                                    class="fa fa-google"></i> Google</a>
                                            <a href="{{ url('login/facebook') }}"
                                               class="btn btn-social-icon btn-facebook"><i
                                                    class="fa fa-facebook"></i> Facebook</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        {{-- Doctor Register Form --}}
                        <div class="tab-pane fade" id="doctor" role="tabpanel" aria-labelledby="doctor-tab">
                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group row mb-3">
                                    </div>

                                    <div class="form-group row">
                                        <label for="name"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                                   value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email"
                                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="birthday"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Birthday') }}</label>

                                        <div class="col-md-6">
                                            <input id="birthday" type="date"
                                                   class="form-control @error('birthday') is-invalid @enderror"
                                                   name="birthday" for="birthday"
                                                   value="{{ old('birthday') }}" required autocomplete="birthday"
                                                   autofocus>

                                            @error('birthday')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="address"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address') }}" required autocomplete="address"
                                                   autofocus>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone_nr"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                        <div class="col-md-6">
                                            <input id="phone_nr" type="text"
                                                   class="form-control @error('phone_nr') is-invalid @enderror"
                                                   name="phone_nr"
                                                   value="{{ old('phone_nr') }}" required autocomplete="phone_nr"
                                                   autofocus>

                                            @error('phone_nr')
                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="password"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password"
                                                   required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="photo"
                                               class="col-md-4 col-form-label text-md-right">{{__('Specialty')}}</label>

                                        <div class="col-md-6">
                                            <select name="spec" class="custom-select" id="inputGroupSelect01">
                                                @foreach($specialties as $spec)

                                                    <option for="spec" type="text" required id="spec" selected>
                                                        {{$spec->specialty}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>



{{--                                    <div class="form-group row">--}}
{{--                                        <label for="photo"--}}
{{--                                               class="col-md-4 col-form-label text-md-right">{{__('Photo')}}</label>--}}

{{--                                        <div class="col-md-6">--}}
{{--                                            <input id="photo" type="file" class="file" name="photo">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}



                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary" name="registerDoctor"
                                                    value="registerDoctor">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#7dc2af"/>
        </svg>
    </div>
    @include('layouts.scriptimp')

@endsection


