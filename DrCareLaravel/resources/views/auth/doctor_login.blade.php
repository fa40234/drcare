@extends('layouts.header')


@include('layouts.headimp')


@section('content')
    <link rel="stylesheet" href="/styles/profile.css">
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
             data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">Login Form</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span>Login <i
                                class="ion-ios-arrow-forward"></i></span></p>
                </div>
            </div>
        </div>
    </section>

    <div class="form-group row mb-4">
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    {{--                    <div class="card-header text-md-center">{{ __('LOG IN') }}</div>--}}

                    <div class="col-md-6">
                        <div class="profile-head">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="login" role="tab"
                                       aria-controls="home" aria-selected="true">User</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#"
                                       role="tab"
                                       aria-controls="profile" aria-selected="false">Login As Doctor</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {{--                    <div class="tab-content profile-tab" id="myTabContent">--}}
                    {{--                        <div class="tab-pane fade show active" id="doctor" role="tabpanel" aria-labelledby="doctor-tab">--}}
                    <div class="card-body">
                        <form method="POST" action="{{ route('doctor.login.submit') }}">
                            @csrf
                            <div class="form-group row mb-3">
                            </div>
                            <div class="form-group row">
                                <label for="email"
                                       class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"
                                                   name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                            </div>
                            <div class="form-group row ">
                                <label class="col-md-6 col-form-label text-md-right">Or login with</label>
                                <div class="row mb-5">
                                </div>
                                <div class="col-md-8 offset-md-4">
                                    <a href="{{ url('login/google') }}"
                                       class="btn btn-social-icon btn-google"><i
                                            class="fa fa-google"></i> Google</a>
                                    <a href="{{ url('login/facebook') }}"
                                       class="btn btn-social-icon btn-facebook"><i
                                            class="fa fa-facebook"></i> Facebook</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>

    @include('layouts.footer')
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#7dc2af"/>
        </svg>
    </div>


    @include('layouts.scriptimp')

@endsection
