@extends('layouts.header')

@include('layouts.headimp')

@section('content')


    <link rel="stylesheet" href="/styles/profile.css">


    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
             data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">Edit Profile</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span>Edit Profile <i
                                class="ion-ios-arrow-forward"></i></span></p>
                </div>
            </div>
        </div>
    </section>
    <div class="container emp-profile">
        @if(count($errors) > 0){
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{route('edit.profile',Auth::user()->id)}}" method="post">
            @csrf
            <input type="hidden" name="_method" value="PATCH"/>
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52y5aInsxSm31CvHOFHWujqUx_wWTS9iM6s7BAm21oEN_RiGoog"
                            alt=""/>
                        <div class="file btn btn-lg btn-primary">
                            Change Photo
                            <input type="file" name="file"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h5>
                            {{ucfirst(Auth::user()->name)}}
                        </h5>

                        <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true">Profile</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Full Name </label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="text" name="name" class="form-control"
                                              value=" {{ucfirst(Auth::user()->name)}} "
                                              placeholder="Enter Name"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="text" name="email" class="form-control" readonly
                                              value="{{Auth::user()->email}} "/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Birthday</label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="date" name="birthday" class="form-control"
                                              value="{{Auth::user()->birthday}}"
                                              placeholder="Enter Birthday"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address</label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="text" name="address" class="form-control"
                                              value="{{Auth::user()->address}}"
                                              placeholder="Enter Address"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Contact Number</label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="text" name="phone_nr" class="form-control"
                                              value="{{Auth::user()->phone_nr}}"
                                              placeholder="Enter Your Phone Number"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Personal Number</label>
                                </div>
                                <div class="col-md-5">
                                    <p><input type="text" name="email" class="form-control"
                                              value="{{Auth::user()->personalNumber}}"
                                              placeholder="Enter Your Personal Number"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <p><input type="submit" class="btn btn-primary" name="btnAddMore"
                                          value="Edit Profile"/></p>
                                <a class="btn btn-secondary" href="{{ route('profile') }}">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


    @include('layouts.footer')


    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#7dc2af"/>
        </svg>
    </div>


    @include('layouts.scriptimp')

@endsection


