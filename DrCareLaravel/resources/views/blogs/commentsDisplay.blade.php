@foreach($comments as $comment)

    <div  @if($comment->parent_id != null) style="margin-left:40px;" @endif>

        <strong>{{ $comment->user->name }}</strong>

        <p>{{ $comment->body }}</p>

        <a href="" id="reply"></a>

        <form method="post" action="{{ route('comment.store',$blog->id) }}">

            @csrf

            <div >

                <input type="text" name="body"  />

                <input type="hidden" name="blog_id" value="{{ $blog_id }}" />

                <input type="hidden" name="parent_id" value="{{ $comment->id }}" />

            </div>

            <div >

                <input type="submit"  value="Reply" />

            </div>

        </form>

        @include('posts.commentsDisplay', ['comments' => $comment->replies])

    </div>

@endforeach
