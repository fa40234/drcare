<!DOCTYPE html>
@extends('layouts.app')
<script src="{{URL::to('/src/js/vendor/tinymce/js/tinymce/tinymce.min.js')}}"></script>
@section('content')

    <div class="container">
        @can('isAdmin')
        <form action="{{ route('update_blog_path', [$blog->id])}}" method="post">
            @csrf
            @method('put')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" value="{{$blog->title}}">


            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" class="form-control" rows="10">{{$blog->content}}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-primary">Edit Blog Post</button>
                <a href="{{route('blogs_path')}}" class="btn btn-outline-secondary">Back</a>
            </div>
        </form>
        @endcan
            @guest
                <div class="alert alert-danger" role="alert">
                    Pershendetje, ju nuk keni akses ne kete faqe.
                </div>
                <a href="/"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kthehuni prapa</button></a>
            @endguest

    </div>



@endsection
<script>
    var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector : "textarea",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.grtElementByTagName('body')[0].clientHeight;
            var cmsURL = editor_config.path_absolute+'laravel-filemanaget?field_name'+field_name;
            if (type = 'image') {
                cmsURL = cmsURL+'&type=Images';
            } else {
                cmsUrl = cmsURL+'&type=Files';
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizeble : 'yes',
                close_previous : 'no'
            });
        }
    };

    tinymce.init(editor_config);
</script>
</html>

