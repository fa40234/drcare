@extends('layouts.app')

@section('content')

    <div class="container">
        <br>
        @can('isAdmin')
            <div class="row">
                @foreach($blogs as $blog)

                    <div class="col-md-6">
                        <div class="card shadow-sm">
                            <div class="card-header bg-primary">
                                <h3 class="text-light">Blog id - {{$blog->id}}</h3>
                            </div>

                            <div class="card-body">
                                <h3>{{$blog->title}}</h3>

                                <h5>{!! Str::limit($blog->content, 100, '...') !!}</h5>
                                <br>

                                <br>
                                <form action="{{route('delete_blog_path', [$blog->id])}}" method="post">
                                    <a href="{{route('edit_blog_path',  [ $blog->id])}}"
                                       class="btn btn-outline-primary">Edit</a>
                                    <a href="{{route('blog_path', [$blog->id])}}" class="btn btn-outline-secondary">View
                                        Post</a>

                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                                </form>
                                <br>
                                <br>
                                <p class="lead">
                                    Posted
                                    {{$blog->created_at->diffForHumans()}}
                                </p>

                            </div>

                        </div>

                    </div>
                @endforeach
            </div>


        @endcan
        @guest
            <div class="alert alert-danger" role="alert">
                Pershendetje, ju nuk keni akses ne kete faqe.
            </div>
            <a href="/">
                <button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kthehuni
                    prapa
                </button>
            </a>
        @endguest
    </div>



@endsection
