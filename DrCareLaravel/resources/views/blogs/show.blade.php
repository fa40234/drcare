<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.headimp')
</head>
<body>
<!-- NAV -->
<@include('layouts.header')
<!-- END nav -->

<section class="hero-wrap hero-wrap-2" style="background-image: url(/images/bg_1.jpg);"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">Care Programss</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i class="ion-ios-arrow-forward"></i></a></span>
                    <span>Care programs <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>

<br>
<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate">
                <h2 class="mb-3">{{$blog->title}}</h2>
                <p>
                    <img src="{{$blog->image}}" alt="" class="img-fluid w-100">
                </p>
                <p> {!! $blog->content !!}</p>

                <div class="tag-widget post-tag-container mb-5 mt-5">
                    <div class="tagcloud">
                        <a href="#" class="tag-cloud-link">Life</a>
                        <a href="#" class="tag-cloud-link">Sport</a>
                        <a href="#" class="tag-cloud-link">Tech</a>
                        <a href="#" class="tag-cloud-link">Travel</a>
                    </div>
                </div>
                <div class="mb-3">
                    <h4>Author</h4>
                </div>
                <div class="about-author d-flex p-4 bg-light">
                    <div class="bio mr-5">
                        <img src="/images/person_1.jpg" alt="Image placeholder" class="img-fluid mb-4">
                    </div>
                    <div class="desc">
                        <h3>George Washington</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem
                            necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente
                            consectetur similique, inventore eos fugit cupiditate numquam!</p>
                    </div>
                </div>


                <div class="pt-5 mt-5">
                    <h3 class="mb-5 h4 font-weight-bold">Comment </h3>
                    {{--                    @foreach($blog->comments as $comment)--}}
                    <ul class="comment-list">
{{--                        @include('blogs.commentsDisplay', ['comments' => $blog->comments, 'blog_id' => $blog->id])--}}

                        @foreach($blog->comments as $comment)
                            <li class="comment">
                                <div class="vcard bio">
                                    <img src="{{$comment->user->photo}}" alt="Image placeholder">
                                </div>
                                <div class="comment-body">
                                    <h3>{{$comment->user->name}}</h3>
                                    <div class="meta mb-2">{{$comment->created_at->diffForHumans()}}</div>
                                    <p>{{$comment->comment}}</p>
                                    <p><a href="#" class="reply">Reply</a></p>
                                </div>
                            </li>
                        @endforeach

                        <li class="comment">
                            <div class="vcard bio">
                                <img src="/images/about.jpg" alt="Image placeholder">
                            </div>
                            <div class="comment-body">
                                <h3>John Doe</h3>
                                <div class="meta mb-2">January 03, 2019 at 2:21pm</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum
                                    necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim
                                    sapiente
                                    iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                                <p><a href="#" class="reply">Reply</a></p>
                            </div>
                        </li>

                    </ul>
                {{--                @endforeach--}}
                <!-- END comment-list -->


                    <div class="comment-form-wrap pt  -5">

                        <h3 class="mb-5 h4 font-weight-bold">Leave a comment</h3>
                        @guest
                            <p>For post a new comment. You need to login first. <a href="{{ route('login') }}">Login</a>
                            </p>
                        @else
                            <form method="post" action="{{ route('comment.store',$blog->id) }}">
                                @csrf
                                {{--                            <div class="form-group">--}}
                                {{--                                <label for="name">Name *</label>--}}
                                {{--                                <input type="text" class="form-control" id="name">--}}
                                {{--                            </div>--}}
                                {{--                            <div class="form-group">--}}
                                {{--                                <label for="email">Email *</label>--}}
                                {{--                                <input type="email" class="form-control" id="email">--}}
                                {{--                            </div>--}}
                                {{--                            <div class="form-group">--}}
                                {{--                                <label for="website">Website</label>--}}
                                {{--                                <input type="url" class="form-control" id="website">--}}
                                {{--                            </div>--}}

                                <div class="form-group">
                                    <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"
                                              aria-required="true" placeholder="Enter Your Comment"
                                              aria-invalid="false"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn py-3 px-4 btn-primary" type="submit" id="form-submit"><b>POST
                                                COMMENT</b></button>
                                    </div><!-- col-sm-12 -->
                                </div>

                            </form>
                        @endguest
                    </div>


                </div>
            </div> <!-- .col-md-8 -->

            <div class="col-lg-4 sidebar ftco-animate">
                <div class="sidebar-box">
                    <form action="#" class="search-form">
                        <div class="form-group">
                            <span class="icon icon-search"></span>
                            <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                        </div>
                    </form>
                </div>
                <div class="sidebar-box ftco-animate">
                    <h3>Category</h3>
                    <ul class="categories">
                        <li><a href="#">Neurology <span>(6)</span></a></li>
                        <li><a href="#">Cardiology <span>(8)</span></a></li>
                        <li><a href="#">Surgery <span>(2)</span></a></li>
                        <li><a href="#">Dental <span>(2)</span></a></li>
                        <li><a href="#">Ophthalmology <span>(2)</span></a></li>
                    </ul>
                </div>

                {{--                <div class="sidebar-box ftco-animate">--}}
                {{--                    <h3>Popular Articles</h3>--}}
                {{--                    <div class="block-21 mb-4 d-flex">--}}
                {{--                        <a class="blog-img mr-auto" style="background-image: url(/images/image_1.jpg);"></a>--}}
                {{--                        <div class="text">--}}
                {{--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the--}}
                {{--                                    blind texts</a></h3>--}}
                {{--                            <div class="meta">--}}
                {{--                                <div><a href="#"><span class="icon-calendar"></span> Oct. 04, 2018</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="block-21 mb-2 d-flex">--}}
                {{--                        <a class="blog-img mr-4" style="background-image: url(/images/image_2.jpg);"></a>--}}
                {{--                        <div class="text">--}}
                {{--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the--}}
                {{--                                    blind texts</a></h3>--}}
                {{--                            <div class="meta">--}}
                {{--                                <div><a href="#"><span class="icon-calendar"></span> Oct. 04, 2018</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="block-21 mb-4 d-flex">--}}
                {{--                        <a class="blog-img mr-4" style="background-image: url(/images/image_3.jpg);"></a>--}}
                {{--                        <div class="text">--}}
                {{--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the--}}
                {{--                                    blind texts</a></h3>--}}
                {{--                            <div class="meta">--}}
                {{--                                <div><a href="#"><span class="icon-calendar"></span> Oct. 04, 2018</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-person"></span> Dave Lewis</a></div>--}}
                {{--                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                <div class="sidebar-box ftco-animate">
                    <h3>Tag Cloud</h3>
                    <ul class="tagcloud m-0 p-0">
                        <a href="#" class="tag-cloud-link">Medical</a>
                        <a href="#" class="tag-cloud-link">Hospital</a>
                        <a href="#" class="tag-cloud-link">Nurse</a>
                        <a href="#" class="tag-cloud-link">Doctor</a>
                        <a href="#" class="tag-cloud-link">Medicine</a>
                        <a href="#" class="tag-cloud-link">Facilities</a>
                        <a href="#" class="tag-cloud-link">Staff</a>
                    </ul>
                </div>

                {{--                <div class="sidebar-box ftco-animate">--}}
                {{--                    <h3>Archives</h3>--}}
                {{--                    <ul class="categories">--}}
                {{--                        <li><a href="#">December 2018 <span>(30)</span></a></li>--}}
                {{--                        <li><a href="#">Novemmber 2018 <span>(20)</span></a></li>--}}
                {{--                        <li><a href="#">September 2018 <span>(6)</span></a></li>--}}
                {{--                        <li><a href="#">August 2018 <span>(8)</span></a></li>--}}
                {{--                    </ul>--}}
                {{--                </div>--}}


                <div class="sidebar-box ftco-animate">
                    <h3>Paragraph</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus
                        voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur
                        similique, inventore eos fugit cupiditate numquam!</p>
                </div>
            </div><!-- END COL -->
        </div>
    </div>
</section>

@include('layouts.footer')
<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#7dc2af"/>
    </svg>
</div>


@include('layouts.scriptimp')

</body>
</html>
