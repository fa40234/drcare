{{--@if(Auth::guard('admin')->check())
    Hello {{Auth::guard('admin')->user()->name}}
@elseif(Auth::guard('user')->check())
    Hello {{Auth::guard('user')->user()->name}}
@endif--}}
    <!doctype html>
<html lang="en">
<head>

</head>
<body>

<div id="header" >
    <nav class="navbar py-4 navbar-expand-lg ftco_navbar navbar-light bg-light flex-row">
        <div class="container">
            <div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
                <div class="col-lg-2 pr-4 align-items-center">
                    <a class="navbar-brand" href="index.html">Dr.<span>care</span></a>
                </div>
                <div class="col-lg-10 d-none d-md-block">
                    <div class="row d-flex">
                        <div class="col-md-4 pr-4 d-flex topper align-items-center">
                            <div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span
                                    class="icon-map"></span></div>
                            <span class="text">Address: Ulpiana, Prishtinë</span>
                        </div>
                        <div class="col-md pr-4 d-flex topper align-items-center">
                            <div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span
                                    class="icon-paper-plane"></span></div>
                            <span class="text">Email: drcare@gmail.com.com</span>
                        </div>
                        <div class="col-md pr-4 d-flex topper align-items-center">
                            <div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span
                                    class="icon-phone2"></span></div>
                            <span class="text">Phone: 049123456</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container d-flex align-items-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                    aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>
            <p class="button-custom order-lg-last mb-0"><a href="" class="btn btn-secondary py-2 px-3">Make
                    An Appointment</a></p>
            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"><a href="{{ route('home') }}" class="nav-link pl-0">Home</a></li>
                    <li class="nav-item"><a href="about" class="nav-link">About</a></li>
                    <li class="nav-item"><a href="/doctor" class="nav-link">Doctor</a></li>
                    <li class="nav-item"><a href="/department" class="nav-link">Departments</a></li>
                    <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>
                    <li class="nav-item"><a href="/careprogram" class="nav-link">Care Program</a></li>
                    @can('isAdmin')
                        <li class="nav-item"><a href="{{route('blogs_path')}}" class="nav-link">Blog Creation</a></li>
                    @endcan

                    @auth('doctor')
                        <li class="nav-item"><a
                                href="{{route('doctorProfile_path', [Auth::guard('doctor')->user()->id])}}"
                                class="nav-link">Profile</a></li>
                        <li class="nav-item dropdown">
                        <li class="nav-item"><a
                                href="{{route('docappointment', [Auth::guard('doctor')->user()->id])}}}"
                                class="nav-link">Appointments</a></li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="javascript:;"
                                   onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('doctor.logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @elseguest()
                        <li class="nav-item"><a href="login" class="nav-link">{{ __('Log in') }}</a></li>
                        @if(Route::has('register'))
                            <li class="nav-item"><a href="register"
                                                    class="nav-link">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item"><a href="{{ route('profile') }}" class="nav-link">Profile</a></li>
                        <li class="nav-item"><a href="{{ route('medicalid') }}" class="nav-link">Medical Id</a></li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

                    @endauth


                    {{--<li class="nav-item"><a href="careprogram" class="nav-link">CareProgram</a></li>--}}
                </ul>
            </div>
        </div>
    </nav>


    @yield('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
    @endif

</div>
</body>
</html>
