@extends('layouts.header')

@include('layouts.headimp')

@section('content')


    <link rel="stylesheet" href="/styles/profile.css">

    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
             data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-2 bread">Profile</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span>Profile <i
                                class="ion-ios-arrow-forward"></i></span></p>
                </div>
            </div>
        </div>
    </section>
    <div class="container emp-profile">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

        <form method="post">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        <img
                            src="/images/doc-3.jpg"
                            alt=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h5>

                     {{$doctor->name}}

                        </h5>

                        <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                   aria-controls="home" aria-selected="true">Profile</a>
                            </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
                                                     </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    {{--                    <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>--}}

                    <a class="profile-edit-btn" href="edit">Edit Profile</a>
                    @can('isUser')
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Le takim
                        </button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Krijo Rezervimin</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="{{route('appointment.store', $doctor->id)}}" >

                                            @csrf
                                            <div class="form-group">
                                                <input type="date" name="date" class="form-control" placeholder="Data">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="arsyeja" class="form-control" placeholder="Arsyeja">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="adresa" class="form-control" placeholder="Adresa">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="mosha" class="form-control" placeholder="Mosha">
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary py-3 px-5">Bej rezervimin</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mbyll</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endcan
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {{--                    <div class="profile-work">--}}
                    {{--                        <p>WORK LINK</p>--}}
                    {{--                        <a href="">Website Link</a><br/>--}}
                    {{--                        <a href="">Bootsnipp Profile</a><br/>--}}
                    {{--                        <a href="">Bootply Profile</a>--}}
                    {{--                        <p>SKILLS</p>--}}
                    {{--                        <a href="">Web Designer</a><br/>--}}
                    {{--                        <a href="">Web Developer</a><br/>--}}
                    {{--                        <a href="">WordPress</a><br/>--}}
                    {{--                        <a href="">WooCommerce</a><br/>--}}
                    {{--                        <a href="">PHP, .Net</a><br/>--}}
                    {{--                    </div>--}}
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Full Name </label>
                                </div>
                                <div class="col-md-6">
                                                                        <p>{{$doctor->name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                                                        <p>{{$doctor->email}} </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Birthday</label>
                                </div>
                                <div class="col-md-6">
                                                                        <p>{{$doctor->birthday}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address</label>
                                </div>
                                <div class="col-md-6">
                                                                        <p>{{$doctor->address}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Contact Number</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$doctor->phone_nr}}</p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </form>
    </div>

    @include('layouts.footer')


    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#7dc2af"/>
        </svg>
    </div>


    @include('layouts.scriptimp')


@endsection


