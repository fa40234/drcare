<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.headimp')
</head>
<body>
@include('layouts.header')
<!-- END nav -->

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <h1 class="mb-2 bread">List of doctors</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="home">Home <i class="ion-ios-arrow-forward"></i></a></span>
                    <span>doctor <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
    </div>
</section>


<div class="container">
    <form action= "/search" method="get">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search Doctor" name="search">
            <div class="input-group-btn">
                <button class="btn-primary" type="submit">Search</button>
            </div>
        </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row">
            @foreach($doctors as $doctor)
                <div class="col-md-6 col-lg-3 ftco-animate">

                    <div class="staff">


                        <div class="img-wrap d-flex align-items-stretch">
                            <div class="img align-self-stretch"
                                 style="background-image: url(/images/doc-{{$loop->iteration}}.jpg);" href="{{route('doctorProfile_path', [$doctor->id])}}"></div>
                            <a href="{{route('doctorProfile_path', [$doctor->id])}}"><img src="/images/doc-{{$loop->iteration}}.jpg" alt="" style="width: 300px;"></a>
                        </div>

                        <div class="text pt-3 text-center">


                            <span class="position mb-2"></span>
                            <div class="faded">
                                <h3><a href="{{route('doctorProfile_path', [$doctor->id])}}">{{$doctor->name}}</a></h3>
                                <p>{{Str::limit($doctor->email, 50, '...')}}</p>

                                <ul class="ftco-social text-center">
                                    <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                </ul>
                                <a href="{{route('doctorProfile_path', [$doctor->id])}}"
                                   class="btn btn-outline-secondary">Read More</a>

                            </div>


                        </div>

                    </div>

                </div>
            @endforeach
        </div>

    </div>
</section>

@include('layouts.footer')
<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#7dc2af"/>
    </svg>
</div>


@include('layouts.scriptimp')

</body>
</html>
